package login;

import ApiTests.commons.commonTypes;

import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


/**
 * Created by obaskirt on 23-Jan-18.
 */
public class BaseTest {

    String accountId= "";
    String token= "";

    @BeforeClass
    public void login () {

        RestAssured.baseURI = commonTypes.URL_PROYECT.getKey();
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        JsonObject loginCredentials = new JsonObject();
        loginCredentials.addProperty("username", "digitalharbor@yopmail.com");
        loginCredentials.addProperty("password", "Password1!");
        httpRequest.body(loginCredentials.toString());
        Response response = httpRequest.post("/api/security/auth");
        accountId =response.headers().getValue("Account-ID");
        token =response.headers().getValue("JWT-Token");


    }

}
