package ApiTests.directory;

import ApiTests.commons.commonTypes;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import login.dataLogin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


@FixMethodOrder(MethodSorters.NAME_ASCENDING) //For Ascending order test execution
public class person {

    String accountId= "";
    String token= "";


    @Before
    public void login () {

        RestAssured.baseURI = commonTypes.URL_PROYECT.getKey();
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        JsonObject loginCredentials = new JsonObject();
        loginCredentials.addProperty("username", dataLogin.USER_NAME.getKey());
        loginCredentials.addProperty("password", dataLogin.PASSWORD.getKey());
        httpRequest.body(loginCredentials.toString());
        Response response = httpRequest.post("/api/security/auth");
        accountId =response.headers().getValue("Account-ID");
        token =response.headers().getValue("JWT-Token");

    }


    @Test
    public void createPerson() {

        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
                jsonAsMap.put("birthday", "1929-01-07");
                jsonAsMap.put("telecomChannels", null);
                jsonAsMap.put("cityId","");
                jsonAsMap.put("countryId",null);
                jsonAsMap.put("education","LIC.");
                jsonAsMap.put("firstName","TESTING");
                jsonAsMap.put("houseNumber","1000");
                jsonAsMap.put("isCustomer",true);
                jsonAsMap.put("isSupplier",false);
                jsonAsMap.put("keywords","Testing with RestAsured");
                jsonAsMap.put("languageId",null);
                jsonAsMap.put("lastName","NEXUS");
                jsonAsMap.put("salutationId",null);
                jsonAsMap.put("shortName","HCW");
                jsonAsMap.put("street","siempre viva");
                jsonAsMap.put("titleId",null);
                jsonAsMap.put("city",null);
                jsonAsMap.put("zip","");


        Response fieldResponse =  given ()
                .auth().oauth2(token)
                .header("Account-ID", accountId)
                .contentType("application/json")
                .body(jsonAsMap).with()
                .when()
                .post(RestAssured.baseURI+commonTypes.CREATE_PERSON_URL.getKey())
                .then()
                .assertThat()
                .log().ifError()
                .statusCode(200)
                .extract().response();
        Assert.assertFalse(fieldResponse.asString().contains("isError"));
    }
}
