package ApiTests.directory;

import ApiTests.commons.commonTypes;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import login.dataLogin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


@FixMethodOrder(MethodSorters.NAME_ASCENDING) //For Ascending order test execution
public class company {

    String accountId= "";
    String token= "";


    @Before
    public void login () {

        RestAssured.baseURI = commonTypes.URL_PROYECT.getKey();
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        JsonObject loginCredentials = new JsonObject();
        loginCredentials.addProperty("username", dataLogin.USER_NAME.getKey());
        loginCredentials.addProperty("password", dataLogin.PASSWORD.getKey());
        httpRequest.body(loginCredentials.toString());
        Response response = httpRequest.post("/api/security/auth");
        accountId =response.headers().getValue("Account-ID");
        token =response.headers().getValue("JWT-Token");

    }


    @Test
    public void createCompany() {

        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
                jsonAsMap.put("cityId","");
                jsonAsMap.put("countryId",null);
                jsonAsMap.put("foundationDate","2019-05-07");
                jsonAsMap.put("houseNumber","4010110");
                jsonAsMap.put("isCustomer",false);
                jsonAsMap.put("isSupplier",true);
                jsonAsMap.put("keywords","keyword");
                jsonAsMap.put("languageId",null);
                jsonAsMap.put("name1","DIGITAL");
                jsonAsMap.put("name2",null);
                jsonAsMap.put("shortName","COMPA");
                jsonAsMap.put("street","calle tupac amary");
                jsonAsMap.put("zip","");
                jsonAsMap.put("city",null);
                jsonAsMap.put("number",null);
                jsonAsMap.put("email",null);
                jsonAsMap.put("emailContact",null);
                jsonAsMap.put("phoneOne",null);
                jsonAsMap.put("phoneContactOne",null);
                jsonAsMap.put("phoneTwo",null);
                jsonAsMap.put("phoneContactTwo",null);
                jsonAsMap.put("channelHasChanges",false);
                jsonAsMap.put("telecomChannels",null);


        Response fieldResponse =  given ()
                .auth().oauth2(token)
                .header("Account-ID", accountId)
                .contentType("application/json")
                .body(jsonAsMap).with()
                .when()
                .post(RestAssured.baseURI+commonTypes.CREATE_COMPANY_URL.getKey())
                .then()
                .assertThat()
                .log().ifError()
                .statusCode(200)
                .extract().response();
        Assert.assertFalse(fieldResponse.asString().contains("isError"));
    }
}
