package ApiTests.settings.contacts;

import ApiTests.commons.commonTypes;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import login.dataLogin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


@FixMethodOrder(MethodSorters.NAME_ASCENDING) //For Ascending order test execution
public class contries {

    String accountId= "";
    String token= "";
    String idContrie="";


    @Before
    public void login () {

        RestAssured.baseURI = commonTypes.URL_PROYECT.getKey();
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        JsonObject loginCredentials = new JsonObject();
        loginCredentials.addProperty("username", dataLogin.USER_NAME.getKey());
        loginCredentials.addProperty("password", dataLogin.PASSWORD.getKey());
        httpRequest.body(loginCredentials.toString());
        Response response = httpRequest.post("/api/security/auth");
        accountId =response.headers().getValue("Account-ID");
        token =response.headers().getValue("JWT-Token");

    }


    @Test
    public void createContries() {

        Map<String, Object> jsonAsMap = new HashMap<String, Object>();
        jsonAsMap.put("name", "Italia");
        jsonAsMap.put("areaCode", "010");
        Response fieldResponse =  given ()
                .auth().oauth2(token)
                .header("Account-ID", accountId)
                .contentType("application/json")
                .body(jsonAsMap).with()
                .when()
                .post(RestAssured.baseURI+commonTypes.CONTRIES_URL.getKey())
                .then()
                .assertThat()
                .log().ifError()
                .statusCode(200)
                .extract().response();
        Assert.assertFalse(fieldResponse.asString().contains("isError"));
        idContrie = fieldResponse.jsonPath().getString("id");

    }
}
