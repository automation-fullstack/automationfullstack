package ApiTests.settings.contacts;

import ApiTests.commons.commonTypes;
import com.google.gson.JsonObject;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import io.restassured.http.ContentType;
import login.dataLogin;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.Map;


import static com.jayway.restassured.RestAssured.given;


@FixMethodOrder(MethodSorters.NAME_ASCENDING) //For Ascending order test execution
public class employee {

    String accountId= "";
    String token= "";
    String idContrie="";
    private Map<String, Object> jsonAsMap = new HashMap<String, Object>();

    /*
    @Before
    public void login () {

        RestAssured.baseURI = commonTypes.URL_PROYECT.getKey();
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");
        JsonObject loginCredentials = new JsonObject();
        loginCredentials.addProperty("username", dataLogin.USER_NAME.getKey());
        loginCredentials.addProperty("password", dataLogin.PASSWORD.getKey());
        httpRequest.body(loginCredentials.toString());
        Response response = httpRequest.post("/api/security/auth");
        accountId =response.headers().getValue("Account-ID");
        token =response.headers().getValue("JWT-Token");

    }
    */

    @Before
    public void setUp() {
        jsonAsMap.put("deleted", true);
        jsonAsMap.put("email", "susy2@jw.org");
        jsonAsMap.put("firstName", "susy2");
        jsonAsMap.put("lastName", "flores");
        jsonAsMap.put("position", "010");
    }

    @Test
    public void createEmployee() {

        String value = commonTypes.URL_PROYECT.getKey()+commonTypes.EMPLOYEE_URL.getKey();
        System.out.print("value" + value);


        Response fieldResponse =  given ()
                .contentType("application/json")
                .body(jsonAsMap).with()
                .when()
                .post(commonTypes.URL_PROYECT.getKey()+commonTypes.EMPLOYEE_URL.getKey())
                .then()
                .assertThat()
                .log().ifError()
                .statusCode(200)
                .extract().response();
        Assert.assertFalse(fieldResponse.asString().contains("isError"));
      //  idContrie = fieldResponse.jsonPath().getString("id");



    }
}
